package org.dxc.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.dxc.Bean.Student;
import org.dxc.factorydesign.ServiceFactory;
import org.dxc.service.StudentService;

public class App {
	public static void main(String[] args) throws IOException {
		Scanner scan = new Scanner(System.in);
		System.out.println("Welcome to student application");
		System.out.println();

		System.out.println("Press 1 -> to insert StudentRecord");
		System.out.println("Press 2 -> to get Student Details");
		System.out.println("Press 3 -> to list Student Details");
		System.out.println("Press 4 -> to delete Record");
		System.out.println("Press 5 -> to update Record");
		System.out.println("Press 6 -> to Exit App");

		int selection = scan.nextInt();
		int sid = 0;
		String sname;
		String saddr;
		Student std = new Student();
		StudentService service = ServiceFactory.getServiceObject();

		switch (selection) {
		case 1:
			System.out.println("Student name: ");
			sname = scan.next();
			System.out.println("Student address: ");
			saddr = scan.next();
			std.setSname(sname);
			std.setSaddr(saddr);

			System.out.println((service.insert(std) != 0) ? "Student Details Uploaded" : "Insertion Failed");

			scan.close();
			break;
		case 2:
			System.out.println("Student id: ");
			sid = scan.nextInt();
			std.setSid(sid);

			std = service.getById(sid);
			System.out.print("Student Name: " + std.getSname());
			System.out.println("  Address: " + std.getSaddr());

			break;
		case 3:
			List<Student> employees = service.getAllStudent();
			for (Iterator iterator = employees.iterator(); iterator.hasNext();) {
				Student student = (Student) iterator.next();
				System.out.print("Student Id: " + student.getSid());
				System.out.print("  Student Name: " + student.getSname());
				System.out.println("  Address: " + student.getSaddr());
			}
			break;
		case 4:
			System.out.println("Student id to be deleted: ");
			sid = scan.nextInt();
			std.setSid(sid);

			service.deleteRecord(sid);
			break;
		case 5:
			System.out.println("Student ID:");
			sid = scan.nextInt();
			std = service.getById(sid);
			BufferedReader b = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Student Name[old name:" + std.getSname() + "]:");
			sname = b.readLine();
			if (sname.equals(null) || sname.equals("")) {
				sname = std.getSname();
			}
			System.out.print("Student Address[old address:" + std.getSaddr() + "]:");
			saddr = b.readLine();
			if (saddr.equals(null) || saddr.equals("")) {
				saddr = std.getSaddr();
			}
			std = new Student();
			std.setSid(sid);
			std.setSname(sname);
			std.setSaddr(saddr);

			service.updateRecord(std);
			break;
		case 6:
			System.out.println("Exited program");
			System.exit(0);
		default:
			System.out.println("Invalid option");
			break;
		}

	}
}
