package org.dxc.service;

import java.util.List;

import org.dxc.Bean.Student;
import org.dxc.factorydesign.HibernateFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class StudentService implements StudentServiceImplementation {

	@Override
	public Integer insert(Student s) {
		// TODO Auto-generated method stub
		Integer empId;

		SessionFactory factory = HibernateFactory.getFactoryObject();

		Session session = factory.openSession();

		Transaction tx = null;

		try {
			tx = session.beginTransaction();

			empId = (Integer) session.save(s);
			tx.commit();
			return empId;
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} finally {
			session.close();
		}
		return null;

	}

	@Override
	public List<Student> getAllStudent() {
		// TODO Auto-generated method stub
		SessionFactory factory = HibernateFactory.getFactoryObject();

		Session session = factory.openSession();

		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			List employees = session.createQuery("FROM Student").list();

			tx.commit();
			return employees;

		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} finally {
			session.close();
		}

		return null;
	}

	@Override
	public Student getById(int id) {
		// TODO Auto-generated method stub
		SessionFactory factory = HibernateFactory.getFactoryObject();

		Session session = factory.openSession();

		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Student student = (Student) session.get(Student.class, id);
			tx.commit();
			return student;
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} finally {
			session.close();
		}
		return null;
	}

	@Override
	public void updateRecord(Student s) {
		// TODO Auto-generated method stub
		SessionFactory factory = HibernateFactory.getFactoryObject();

		Session session = factory.openSession();

		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Student student = (Student) session.get(Student.class, s.getSid());
			student.setSname(s.getSname());
			student.setSaddr(s.getSaddr());
			session.update(student);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

	}

	@Override
	public void deleteRecord(int id) {
		// TODO Auto-generated method stub
		SessionFactory factory = HibernateFactory.getFactoryObject();

		Session session = factory.openSession();

		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Student student = (Student) session.get(Student.class, id);
			session.delete(student);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

}
